/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fdf.h                                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/25 08:35:39 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 23:55:25 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef FDF_H
# define FDF_H

# include "libft.h"
# include "get_next_line.h"
# include "minilibx_macos/mlx.h"
# include <stdio.h>
# include <math.h>
# include <fcntl.h>

# define WIN_HEIGHT 1000
# define WIN_WIDTH 1600
# define MOVE_DIFF 50
# define ZOOM_DIFF 0.2

typedef unsigned char	t_uc;

typedef struct	s_point
{
	float		x;
	float		y;
	float		z;
	float		w_x;
	float		w_y;
	t_uc		color_scale;
}				t_point;

typedef struct	s_config
{
	int			rows;
	int			columns;
	float		min_height;
	float		max_height;
	char		projection_type;
	char		color;
	float		angle;
	float		inclination;
	float		zoom;
	float		x_translation;
	float		y_translation;
	float		step;
	float		height_scale;
	t_point		projected_center;
	t_point		map_center;
}				t_config;

typedef struct	s_environment
{
	void		*mlx;
	void		*win;
	char		*file;
	t_point		**map;
	t_config	*config;
}				t_environment;

void			initialize_environment_structure(t_environment *e,
						int ac, char **av);
void			get_rows_and_columns(t_environment *e);
void			get_coordinates_map(t_environment *e);
t_point			**alloc_memory_coordinates(int rows, int columns);
int				get_next_line(int fd, char **line);
float			get_step(int rows, int columns);
void			draw_axonometry(t_environment *e);
void			draw_perspective(t_environment *e);
void			get_axonometry_coordinates(t_environment *e);
int				key_hook(int keycode, t_environment *e);
int				expose_hook(t_environment *e);
void			get_max_min_height_map(t_environment *e);
int				get_color(char color, t_uc color_scale);
void			draw_line(t_environment *e, t_point m, t_point n);
void			get_perspective_coordinates(t_environment *e);
void			get_map_color_scale(t_environment *e);
void			reset_configurations(t_environment *e);

#endif
