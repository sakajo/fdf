# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2015/11/18 10:05:52 by jfazakas          #+#    #+#              #
#    Updated: 2015/12/02 23:08:59 by jfazakas         ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME = fdf

SRC = src/fdf.c\
	  src/get_next_line.c\
	  src/get_rows_and_columns.c\
	  src/get_coordinates_map.c\
	  src/get_step.c\
	  src/alloc_memory_coordinates.c\
	  src/get_axonometry_coordinates.c\
	  src/draw_axonometry.c\
	  src/key_hook.c\
	  src/expose_hook.c\
	  src/initialize_environment_structure.c\
	  src/get_max_min_height_map.c\
	  src/get_color.c\
	  src/draw_line.c\
	  src/draw_perspective.c\
	  src/get_perspective_coordinates.c\
	  src/get_map_color_scale.c\
	  src/reset_configurations.c\

OBJ = fdf.o\
	  get_next_line.o\
	  get_rows_and_columns.o\
	  get_coordinates_map.o\
	  get_step.o\
	  alloc_memory_coordinates.o\
	  get_axonometry_coordinates.o\
	  draw_axonometry.o\
	  key_hook.o\
	  expose_hook.o\
	  initialize_environment_structure.o\
	  get_max_min_height_map.o\
	  get_color.o\
	  draw_line.o\
	  draw_perspective.o\
	  get_perspective_coordinates.o\
	  get_map_color_scale.o\
	  reset_configurations.o\

INC = includes/

FLAGS = -Wall -Wextra -Werror

MLX = -Lincludes/minilibx_macos -lmlx -framework OpenGL -framework AppKit

LIBFT = -L libft/ -lft

all: $(NAME)

$(NAME):
	make -C libft/
	gcc $(FLAGS) -I $(INC) -c $(SRC)
	gcc $(OBJ) $(MLX) $(LIBFT) -o $(NAME)

clean:
	make -C libft/ clean
	rm -f $(OBJ)

fclean: clean
	make -C libft/ fclean
	rm -f $(NAME)

re: fclean all
