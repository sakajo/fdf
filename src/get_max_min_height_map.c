/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_max_min_height_map.c                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 20:55:31 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 21:31:34 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	get_max_min_height_map(t_environment *e)
{
	float	maximum;
	float	minimum;
	int		row;
	int		col;

	maximum = e->map[0][0].z;
	minimum = maximum;
	row = 0;
	while (row < e->config->rows)
	{
		col = 0;
		while (col < e->config->columns)
		{
			if (e->map[row][col].z < minimum)
				minimum = e->map[row][col].z;
			if (e->map[row][col].z > maximum)
				maximum = e->map[row][col].z;
			col++;
		}
		row++;
	}
	e->config->max_height = maximum;
	e->config->min_height = minimum;
}
