/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_map_color_scale.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 21:50:08 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 21:54:36 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	get_map_color_scale(t_environment *e)
{
	int		row;
	int		col;

	row = 0;
	while (row < e->config->rows)
	{
		col = 0;
		while (col < e->config->columns)
		{
			e->map[row][col].color_scale = 255 - 255 * (e->map[row][col].z -
					e->config->min_height) / (e->config->max_height -
						e->config->min_height);
			col++;
		}
		row++;
	}
}
