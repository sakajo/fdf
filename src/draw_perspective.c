/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_perspective.c                                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 19:10:44 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 22:10:24 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	draw_perspective(t_environment *e)
{
	int		row;
	int		col;

	row = 0;
	while (row < e->config->rows)
	{
		col = 0;
		while (col < e->config->columns)
		{
			if (row < e->config->rows - 1)
				draw_line(e, e->map[row][col], e->map[row + 1][col]);
			if (col < e->config->columns - 1)
				draw_line(e, e->map[row][col], e->map[row][col + 1]);
			col++;
		}
		row++;
	}
}
