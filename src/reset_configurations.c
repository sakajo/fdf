/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   reset_configurations.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 23:05:14 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/03 00:18:20 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	reset_configurations(t_environment *e)
{
	e->config->angle = 0;
	e->config->inclination = 0.7;
	e->config->zoom = 1;
	e->config->x_translation = 0;
	e->config->y_translation = 0;
	e->config->height_scale = 1;
	if (e->config->projection_type == 'a')
		get_axonometry_coordinates(e);
	else
		get_perspective_coordinates(e);
}
