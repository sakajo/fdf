/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   initialize_environment_structure.c                 :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 22:15:16 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 23:14:21 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static char	*get_name(char *path)
{
	int		index;
	int		count;

	index = ft_strlen(path);
	while (index > 0 && path[index - 1] != '/')
		index--;
	count = 0;
	while (path[index + count] != '.' && path[index + count] != '\0')
		count++;
	return (ft_strsub(path, index, count));
}

void		initialize_environment_structure(t_environment *e,
		int ac, char **av)
{
	e->config = (t_config*)malloc(sizeof(t_config));
	if (ac != 2)
		e->file = ft_strdup("maps/42.fdf");
	else
		e->file = av[1];
	get_rows_and_columns(e);
	e->map = alloc_memory_coordinates(e->config->rows, e->config->columns);
	get_coordinates_map(e);
	e->config->step = get_step(e->config->rows, e->config->columns);
	get_max_min_height_map(e);
	get_map_color_scale(e);
	e->mlx = mlx_init();
	e->win = mlx_new_window(e->mlx, WIN_WIDTH, WIN_HEIGHT, get_name(e->file));
	e->config->color = 'r';
	e->config->projection_type = 'a';
	reset_configurations(e);
}
