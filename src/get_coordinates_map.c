/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_coordinates_map.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 18:12:45 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 21:38:58 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	get_coordinates_row(t_environment *e, int row, char *line)
{
	char	**split;
	int		column;
	float	row_y;

	row_y = -(e->config->rows - 1) / 2 + row;
	split = ft_strsplit(line, ' ');
	column = 0;
	while (column < e->config->columns)
	{
		e->map[row][column].x = -(e->config->columns - 1) / 2 + column;
		e->map[row][column].y = row_y;
		e->map[row][column].z = ft_atoi(split[column]);
		column++;
	}
}

void	get_coordinates_map(t_environment *e)
{
	int		fd;
	char	*line;
	int		row;

	fd = open(e->file, O_RDONLY);
	row = 0;
	while (get_next_line(fd, &line) > 0)
	{
		get_coordinates_row(e, row, line);
		row++;
	}
}
