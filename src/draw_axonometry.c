/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_axonometry.c                                  :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 16:31:14 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 22:54:08 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

void	draw_axonometry(t_environment *e)
{
	int		row;
	int		col;
	t_point	**xyz;
	t_point	mid;

	xyz = e->map;
	mid.w_x = WIN_WIDTH / 2;
	mid.w_y = WIN_HEIGHT / 2;
	row = 0;
	while (row < e->config->rows)
	{
		col = 0;
		while (col < e->config->columns)
		{
			if (row < e->config->rows - 1)
				draw_line(e, xyz[row][col], xyz[row + 1][col]);
			if (col < e->config->columns - 1)
				draw_line(e, xyz[row][col], xyz[row][col + 1]);
			col++;
		}
		row++;
	}
}
