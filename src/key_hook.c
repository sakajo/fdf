/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   key_hook.c                                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 15:29:44 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/03 00:22:35 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	camera_key_hook(int keycode, t_environment *e)
{
	if (keycode == 8)
	{
		if (e->config->projection_type == 'a')
		{
			get_perspective_coordinates(e);
			expose_hook(e);
		}
		else
		{
			get_axonometry_coordinates(e);
			expose_hook(e);
		}
	}
}

static void	color_key_hook(int keycode, t_environment *e)
{
	if (keycode == 15)
	{
		e->config->color = 'r';
		expose_hook(e);
	}
	if (keycode == 5)
	{
		e->config->color = 'g';
		expose_hook(e);
	}
	if (keycode == 11)
	{
		e->config->color = 'b';
		expose_hook(e);
	}
	if (keycode == 16)
	{
		e->config->color = 'y';
		expose_hook(e);
	}
}

static void	axonometry_key_hook(int keycode, t_environment *e)
{
	if (keycode == 24 || keycode == 69 || keycode == 27 || keycode == 78 ||
			keycode == 43 || keycode == 47 || (keycode > 122 && keycode < 127))
	{
		if (keycode == 24 || keycode == 69)
			e->config->zoom += ZOOM_DIFF;
		if ((keycode == 27 || keycode == 78) && 0.2 < e->config->zoom)
			e->config->zoom -= ZOOM_DIFF;
		if (keycode == 43)
			e->config->height_scale /= 2;
		if (keycode == 47)
			e->config->height_scale *= 2;
		if (keycode == 123)
			e->config->x_translation -= MOVE_DIFF;
		if (keycode == 124)
			e->config->x_translation += MOVE_DIFF;
		if (keycode == 125)
			e->config->y_translation += MOVE_DIFF;
		if (keycode == 126)
			e->config->y_translation -= MOVE_DIFF;
		get_axonometry_coordinates(e);
		expose_hook(e);
	}
}

static void	perspective_key_hook(int keycode, t_environment *e)
{
	if (keycode >= 123 && keycode <= 126)
	{
		if (keycode == 123)
			e->config->angle -= 0.1;
		if (keycode == 124)
			e->config->angle += 0.1;
		if (keycode == 125 && e->config->inclination > -1)
			e->config->inclination -= 0.1;
		if (keycode == 126 && e->config->inclination < 1)
			e->config->inclination += 0.1;
		get_perspective_coordinates(e);
		expose_hook(e);
	}
	if (keycode == 43 || keycode == 47)
	{
		if (keycode == 43)
			e->config->height_scale /= 2;
		else
			e->config->height_scale *= 2;
		get_perspective_coordinates(e);
		expose_hook(e);
	}
}

int			key_hook(int keycode, t_environment *e)
{
	if (keycode == 53)
		exit(0);
	color_key_hook(keycode, e);
	if (e->config->projection_type == 'a')
		axonometry_key_hook(keycode, e);
	else
		perspective_key_hook(keycode, e);
	camera_key_hook(keycode, e);
	if (keycode == 49)
	{
		reset_configurations(e);
		expose_hook(e);
	}
	return (0);
}
