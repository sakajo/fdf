/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   draw_line.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 16:37:51 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 22:53:48 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static float	get_absolute(float n)
{
	if (n > 0)
		return (n);
	return (-n);
}

static float	get_max(float a, float b)
{
	if (a > b)
		return (a);
	return (b);
}

void			draw_line(t_environment *e, t_point m, t_point n)
{
	float	fractions;
	float	index;
	int		color;

	fractions = get_max(get_absolute(m.w_x - n.w_x),
			get_absolute(m.w_y - n.w_y));
	index = 0;
	while (index <= fractions)
	{
		color = get_color(e->config->color, m.color_scale +
				(n.color_scale - m.color_scale) * index / fractions);
		mlx_pixel_put(e->mlx, e->win,
				m.w_x + index / fractions * (n.w_x - m.w_x),
				m.w_y + index / fractions * (n.w_y - m.w_y), color);
		index++;
	}
}
