/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   alloc_memory_coordinates.c                         :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 16:16:34 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 21:40:42 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

t_point	**alloc_memory_coordinates(int rows, int columns)
{
	t_point	**map;
	int		index;

	map = (t_point**)malloc(sizeof(t_point*) * rows);
	index = 0;
	while (index < rows)
	{
		map[index] = (t_point*)malloc(sizeof(t_point) * columns);
		index++;
	}
	return (map);
}
