/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_axonometry_coordinates.c                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/12/02 21:58:11 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 23:16:23 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static float	get_axonometric_x(float step, t_point point)
{
	float	x;

	x = WIN_WIDTH / 2 + step * point.x - step * point.y;
	return (x);
}

static float	get_axonometric_y(float hs, float step, t_point point)
{
	float	y;

	y = WIN_HEIGHT / 2 + 0.5 * step * point.x + step * point.y -
		0.1 * step * point.z * hs;
	return (y);
}

static void		zoom_and_translate_points(t_environment *e)
{
	int		row;
	int		col;

	row = 0;
	while (row < e->config->rows)
	{
		col = 0;
		while (col < e->config->columns)
		{
			e->map[row][col].w_x += e->config->x_translation;
			e->map[row][col].w_y += e->config->y_translation;
			e->map[row][col].w_x = WIN_WIDTH / 2 + (e->map[row][col].w_x -
					WIN_WIDTH / 2) * e->config->zoom;
			e->map[row][col].w_y = WIN_HEIGHT / 2 + (e->map[row][col].w_y -
					WIN_HEIGHT / 2) * e->config->zoom;
			col++;
		}
		row++;
	}
}

void			get_axonometry_coordinates(t_environment *e)
{
	int		row;
	int		col;

	e->config->projection_type = 'a';
	row = 0;
	while (row < e->config->rows)
	{
		col = 0;
		while (col < e->config->columns)
		{
			e->map[row][col].w_x =
				get_axonometric_x(e->config->step, e->map[row][col]);
			e->map[row][col].w_y = get_axonometric_y(e->config->height_scale,
						e->config->step, e->map[row][col]);
			col++;
		}
		row++;
	}
	if (e->config->x_translation != 0 || e->config->y_translation != 0 ||
			e->config->zoom != 1)
		zoom_and_translate_points(e);
}
