/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   expose_hook.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 15:33:26 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/03 00:23:12 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	print_help_axonometry(t_environment *e)
{
	mlx_string_put(e->mlx, e->win, 20, 20, 0xffffff, "AXONOMETRY:");
	mlx_string_put(e->mlx, e->win, 20, 40, 0xffffff,
			"c: change projection type");
	mlx_string_put(e->mlx, e->win, 20, 60, 0xffffff,
			"r/g/b/y: change color");
	mlx_string_put(e->mlx, e->win, 20, 80, 0xffffff, "+/-: zoom in/out");
	mlx_string_put(e->mlx, e->win, 20, 100, 0xffffff, "arrows: move");
	mlx_string_put(e->mlx, e->win, 20, 120, 0xffffff,
			"</>: change height scale");
	mlx_string_put(e->mlx, e->win, 20, 140, 0xffffff, "SPACE: reset place");
	mlx_string_put(e->mlx, e->win, 20, 160, 0xffffff, "ESC: exit");
}

static void	print_help_perspective(t_environment *e)
{
	mlx_string_put(e->mlx, e->win, 20, 20, 0xffffff, "PERSPECTIVE:");
	mlx_string_put(e->mlx, e->win, 20, 40, 0xffffff,
			"c: change projection type");
	mlx_string_put(e->mlx, e->win, 20, 60, 0xffffff,
			"r/g/b/y: change color");
	mlx_string_put(e->mlx, e->win, 20, 80, 0xffffff, "left/right: rotate");
	mlx_string_put(e->mlx, e->win, 20, 100, 0xffffff,
			"up/down: change inclination");
	mlx_string_put(e->mlx, e->win, 20, 120, 0xffffff,
			"</>: change height scale");
	mlx_string_put(e->mlx, e->win, 20, 140, 0xffffff, "SPACE: reset place");
	mlx_string_put(e->mlx, e->win, 20, 160, 0xffffff, "ESC: exit");
}

int			expose_hook(t_environment *e)
{
	mlx_clear_window(e->mlx, e->win);
	if (e->config->projection_type == 'a')
	{
		draw_axonometry(e);
		print_help_axonometry(e);
	}
	else
	{
		draw_perspective(e);
		print_help_perspective(e);
	}
	return (0);
}
