/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_perspective_coordinates.c                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 19:33:17 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 23:21:03 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static t_point	get_projected_center(double inclination)
{
	t_point	center;

	center.x = 0;
	center.y = -3.73 * 0.3 * WIN_WIDTH;
	center.z = center.y * inclination;
	return (center);
}

static t_point	get_map_center(t_environment *e)
{
	t_point	center;
	float	radius;

	radius = sqrt((e->config->rows - 1) * (e->config->rows - 1) +
			(e->config->columns - 1) * (e->config->columns - 1)) / 2;
	center.x = 0;
	center.y = -radius * 3.864;
	center.z = center.y * e->config->inclination;
	return (center);
}

static t_point	rotate_point(t_point a, double angle)
{
	t_point	rot;

	rot.x = a.x * cos(angle) - a.y * sin(angle);
	rot.y = a.x * sin(angle) + a.y * cos(angle);
	rot.z = a.z;
	return (rot);
}

void			get_perspective_coordinates(t_environment *e)
{
	int		row;
	int		col;
	t_point	point;

	e->config->projection_type = 'p';
	e->config->map_center = get_map_center(e);
	e->config->projected_center = get_projected_center(e->config->inclination);
	row = -1;
	while (++row < e->config->rows)
	{
		col = -1;
		while (++col < e->config->columns)
		{
			point = rotate_point(e->map[row][col], e->config->angle);
			point.w_x = WIN_WIDTH / 2 + (point.x + e->config->map_center.x) *
				e->config->projected_center.y /
				(point.y + e->config->map_center.y);
			point.w_y = WIN_HEIGHT / 2 + e->config->projected_center.z -
				(point.z * 0.1 * e->config->height_scale +
				e->config->map_center.z) * e->config->projected_center.y /
				(point.y + e->config->map_center.y);
			e->map[row][col].w_x = point.w_x;
			e->map[row][col].w_y = point.w_y;
		}
	}
}
