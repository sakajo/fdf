/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_color.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/29 16:36:18 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 20:49:28 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

int		get_color(char color, t_uc color_scale)
{
	t_uc	*rgb;

	rgb = (t_uc*)malloc(sizeof(t_uc) * 4);
	rgb[3] = 0;
	if (color == 'r' || color == 'y')
		rgb[2] = 255;
	else
		rgb[2] = color_scale;
	if (color == 'g' || color == 'y')
		rgb[1] = 255;
	else
		rgb[1] = color_scale;
	if (color == 'b')
		rgb[0] = 255;
	else
		rgb[0] = color_scale;
	return (*(int*)rgb);
}
