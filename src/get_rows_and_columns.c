/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_rows_and_columns.c                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/26 17:38:34 by jfazakas          #+#    #+#             */
/*   Updated: 2015/12/02 21:20:27 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fdf.h"

static void	print_map_error(void)
{
	ft_putendl_fd("Map error!", 2);
	exit(0);
}

void		get_rows_and_columns(t_environment *e)
{
	int		fd;
	char	*line;
	char	**split;
	int		cols;
	int		gnl_ret;

	fd = open(e->file, O_RDONLY);
	e->config->rows = 0;
	e->config->columns = 0;
	while ((gnl_ret = get_next_line(fd, &line)) > 0)
	{
		split = ft_strsplit(line, ' ');
		cols = 0;
		while (split[cols] != 0)
			cols++;
		if (e->config->columns == 0)
			e->config->columns = cols;
		else if (e->config->columns != cols)
			print_map_error();
		e->config->rows++;
	}
	if (e->config->rows == 0)
		print_map_error();
	close(fd);
}
