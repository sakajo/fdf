/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itobin.c                                        :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/19 14:07:05 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/19 14:07:07 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itobin(unsigned int n)
{
	char			*binary;
	int				length;
	unsigned int	copy;

	copy = n;
	length = 1;
	while (copy > 1)
	{
		length++;
		copy /= 2;
	}
	binary = ft_strnew(length);
	while (length > 0)
	{
		length--;
		binary[length] = n % 2 + '0';
		n /= 2;
	}
	return (binary);
}
