/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itooctal.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/19 14:07:26 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/19 14:07:28 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itooctal(unsigned int n)
{
	char			*octal;
	int				length;
	unsigned int	copy;

	copy = n;
	length = 1;
	while (copy > 7)
	{
		length++;
		copy /= 8;
	}
	octal = ft_strnew(length);
	while (length > 0)
	{
		length--;
		octal[length] = n % 8 + '0';
		n /= 8;
	}
	return (octal);
}
