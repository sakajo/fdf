/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   ft_itohexa.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jfazakas <marvin@42.fr>                    +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2015/11/19 14:07:15 by jfazakas          #+#    #+#             */
/*   Updated: 2015/11/19 14:07:17 by jfazakas         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "libft.h"

char	*ft_itohexa(unsigned int n)
{
	char			*hexa;
	int				length;
	unsigned int	copy;

	copy = n;
	length = 1;
	while (copy > 15)
	{
		length++;
		copy /= 16;
	}
	hexa = ft_strnew(length);
	while (length > 0)
	{
		length--;
		if (n % 16 > 9)
			hexa[length] = n % 16 + 'A' - 10;
		else
			hexa[length] = n % 16 + '0';
		n /= 16;
	}
	return (hexa);
}
